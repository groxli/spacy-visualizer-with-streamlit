# Streamlit app using SpaCy visualization tools
This project is a Streamlit app for visualizing NLP dependencies and entities. 

## Dependencies
If you do not have the streamlit and spacy modules installed, you can do so from the command line using pip:

```
pip install spacy
pip install streamlit
```

You will also need to install the SpaCy 'medium' size English language model if it is not already installed:

```
python3 -m spacy download en_core_web_md
```

## Running the app
From the command line where the project directory is located, type:

```
streamlit run visualizer.py
```

To stop the app, type:

```
ctrl + c
```
